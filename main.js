const {City} = require('./City')
const {Capital} = require('./Capital')
const {Country} = require('./Country')

let city1 = new City('Kharkiv')
let city2 = new City('Lviv')

city1.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
city2.setWeather().then(() => {console.log(city2)}).catch((error) => {console.log(error)})

let capital = new Capital('Kyiv')
capital.setAirport('Boryspil')
capital.setWeather().then(() => {console.log(capital)}).catch((error) => {console.log(error)})

let countryNew = new Country('Ukraine', ['Kharkiv', 'Lviv', 'Kyiv', 'Odesa', 'Khmelnyckyy', 'Ternopil', 'Vynnycia', 'Uzhgorod', 'Chop', 'Kherson'])

let index = countryNew.cities.findIndex(cities => cities === 'Lviv')
console.log(index)

countryNew.cities.splice(3,2,['Lazurne', 'Mariupol'])
console.log(countryNew.cities)


countryNew.setWeather().then(() => {console.log(countryNew)}).catch((error) => {console.log(error)})

function temperatureDown(a, b){
    if(a.temperature < b.temperature)
        return 1
    if(a.temperature > b.temperature)
        return -1
    return 0
}
