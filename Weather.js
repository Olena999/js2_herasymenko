const axios = require('axios')

const weatherUrl = 'https://goweather.herokuapp.com/weather/'

class Weather{
    /**
     * Describe attributes: temperature, wind, forecast, date
     */
    constructor() {
        this.temperature = ''
        this.wind = ''
        this.forecast = ''
        this.date = Date.now()
    }

    async setWeather(cityName){
        await axios.get(weatherUrl + cityName).then((response) => {
            this.temperature = response.data.temperature
            this.wind = response.data.wind
        }).catch((error) => {return error.message})
    }

    async setForecast(cityName){
        await axios.get(weatherUrl + cityName).then((response) => {
            this.forecast = response.data.forecast([])
        }).catch((error) => {return error.message})
    }

}

module.exports = {Weather}